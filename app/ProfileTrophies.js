import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
} from 'react-native';
import Swiper from 'react-native-swiper';

class ProfileTrophies extends Component {
  render(){
    return(
      <Swiper
        loop={false}
        showsPagination={false}
        index={0}>
        <View style={styles.profileContainer}>
          <Text style={styles.profileText}>Here is a Profile...</Text>
        </View>
        <View style={styles.trophiesContainer}>
          <Text style={styles.trophiesText}>Here are some Trophies...</Text>
        </View>
      </Swiper>
    )
  }
}

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'mistyrose',
  },
  profileText: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  trophiesContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'olive',
  },
  trophiesText: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

module.exports = ProfileTrophies;
