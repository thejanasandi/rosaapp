import React, { Component } from 'react';
import {
  StyleSheet,
  AppRegistry,
  Text,
  View,
  Dimensions,
  TouchableHighlight,
} from 'react-native';
import MapView from 'react-native-maps';


class Map extends Component {

  onButtonPress(){
      this.props.navigator.push({
        id: 'ProfileTrophies'
      });
    }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
        <TouchableHighlight onPress={this.onButtonPress.bind(this)} style={styles.buttonOnMap}>
          <Text style={styles.buttonText}>Profile</Text>
        </TouchableHighlight>
        <MapView
          style={styles.map}
            initialRegion={{
              latitude: 47.559601,
              longitude: 7.588576,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            region={{
              latitude: 47.559601,
              longitude: 7.588576,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}>
       </MapView>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
   },
   map: {
    ...StyleSheet.absoluteFillObject,
    zIndex: -1,
   },
   buttonOnMap: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    marginLeft: 157,
    marginRight: 157,
    width: 60,
    height: 60,
    borderRadius: 100/2,
    backgroundColor: 'white',
    opacity: 0.8,
    shadowColor: 'darkgrey',
    shadowOffset:{width: 0,height: 0},
    shadowOpacity: 1,
    shadowRadius: 5,
    zIndex: 1,
  },
  buttonText: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 12,
    color: 'black',
  },
});

module.exports = Map;

/*
import SlidingUpPanel from 'react-native-sliding-up-panel';

var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

var MAXIMUM_HEIGHT = deviceHeight - 145;
var MINUMUM_HEIGHT = 50;


constructor(props) {
  super(props);
  this.state = {
    containerHeight : 0
  }
}


<SlidingUpPanel
   ref={panel => { this.panel = panel; }}
   containerMaximumHeight={MAXIMUM_HEIGHT}
   containerBackgroundColor={'white'}
   containerOpacity={0.8}
   handlerHeight={MINUMUM_HEIGHT}
   allowStayMiddle={false}
   handlerDefaultView={<PullUpPanel/>}
   getContainerHeight={this.getContainerHeight}>
   <View style={styles.PullUpShape}/>
   <View style={styles.frontContainer}>
     <Text style={styles.panelText}>Stories of R.G.</Text>
     <Text style={styles.panelText}>Stories of R.G.</Text>
     <Text style={styles.panelText}>Stories of R.G.</Text>
     <Text style={styles.panelText}>Stories of R.G.</Text>
     <Text style={styles.panelText}>Stories of R.G.</Text>
     <Text style={styles.panelText}>Stories of R.G.</Text>
   </View>
 </SlidingUpPanel>


getContainerHeight = (height) => {
    this.setState({
      containerHeight : height
    });
  }
class PullUpPanel extends Component{
  render() {
    return (
        <View style={styles.pullUpBar}/>
    );
  }
};


pullUpBar: {
  flex: 1,
},
PullUpShape: {
  position: 'absolute',
  justifyContent: 'center',
  alignItems: 'center',
  marginTop: 10,
  marginLeft: 170,
  marginRight:0,
  width: 30,
  height: 5,
  backgroundColor: 'dimgrey',
  borderRadius: 5
},
frontContainer: {
  position: 'absolute',
  flex : 1,
  flexDirection: 'column',
  marginTop: 80,
  marginLeft: 145,
  marginRight:0,
  justifyContent: 'center',
  alignItems: 'center',
}, */
