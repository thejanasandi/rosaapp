import React, { Component } from 'react';
import {
  StyleSheet,
  AppRegistry,
  View,
  Navigator,
  Animated,
} from 'react-native';
import MapView from 'react-native-maps';

var Map = require('./app/Map.js');
var ProfileTrophies = require('./app/ProfileTrophies');

class makethree extends Component {
  render() {
    return (
      <Navigator
        initialRoute = {{
          id: 'Map'
        }}
        renderScene = {
          this.navigatorRenderScene
        }
        configureScene = {(route,routeStack) =>
          Navigator.SceneConfigs.VerticalDownSwipeJump}
      />
    );
  }
  navigatorRenderScene(route,navigator){
    _navigator = navigator;
    switch (route.id){
      case 'Map':
        return(<Map navigator = {navigator} title = "Map"/>);
      case 'ProfileTrophies':
        return(<ProfileTrophies navigator = {navigator} title = "ProfileTrophies"/>);
    }
  }
}

AppRegistry.registerComponent('makethree', () => makethree);
